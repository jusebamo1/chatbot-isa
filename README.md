# ChatBot-ISA

## Name
ChatBot-ISA

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation

- En un terminal dentro de ISA_fin:
```

*source bin/activate

*sudo apt install python3-pip

*pip3 install -U --user pip && pip3 install rasa

*python3 -m pip install Django

*pip install pandas

*python3 -m pip install --user -U pyTelegramBotAPI

*pip install datetime

*pip install django-import_export
```

```

*sudo apt install curl

*curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc | sudo tee /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null && echo "deb https://ngrok-agent.s3.amazonaws.com buster main" | sudo tee /etc/apt/sources.list.d/ngrok.list && sudo apt update && sudo apt install ngrok
```
- Si se quiere hacer uso de ngrok sin limite de tiempo se debe de instalar con su cuenta de ngrok en la pagina de ngrok https://ngrok.com/download


```
*cd src

*python3 manage.py makemigrations
*python3 manage.py migrate
*python3 manage.py runserver
```

- Aceder a la pagina con el link dado en el teminal seguido de home ej: http://127.0.0.1:8000/home

En otro terminal dentro de ISA_fin:
```
*ngrok http 5005

```

- Copiar el fowarding desde "https" hasta "ngrok.io" y en el archivo credentials de rasa pegar lo que copió en frente de "webhook_url:" seguido de "/webhooks/telegram/webhook" todo dentro de comillas

Guarde el archivo con "CTRL + s"

Luego de haber hecho el cambio en cedentials se procede a acceder a la pagina y hacer click en el botton de "Actializar ISA"


## Support
Para más información visitar la pagina oficial de rasa.
https://rasa.com/



## Authors and acknowledgment
Juan Sebastina Barragan Monsalve & Richard John Blacker Garcia

## License
Licencia Abierta

## Project status
Status: Prototipo

